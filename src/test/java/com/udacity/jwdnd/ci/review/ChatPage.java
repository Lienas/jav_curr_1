package com.udacity.jwdnd.ci.review;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ChatPage {
    @FindBy(id = "messageText")
    private WebElement messageTextField;

    @FindBy(id = "messageType")
    private WebElement messageTypeField;

    @FindBy(id = "submitMsg")
    private WebElement submitBtn;

    @FindBy(id = "logoutBtn")
    private WebElement logoutBtn;

    @FindAll(@FindBy(className = "msg"))
    private List<WebElement> messageListFields;


    public ChatPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void setMessageTextField(String text) {
        messageTextField.sendKeys(text);
    }

    public void setMessageTypeField(String type) {
        messageTypeField.sendKeys(type);
    }

    public Integer getAmountOfMessages() {
        return messageListFields.size();
    }

    public void submit() {
        submitBtn.click();
    }

    public void logout() {
        logoutBtn.click();
    }

    public String getUserNameOfLastMsg() {
        var count = messageListFields.size();
        var lastMessage = messageListFields.get(count-1);
        var username = lastMessage.findElement(By.tagName("span")).getText();
        return username;
    }
}

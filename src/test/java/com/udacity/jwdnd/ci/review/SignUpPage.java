package com.udacity.jwdnd.ci.review;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {
    @FindBy(id = "inputUsername")
    private WebElement usernameField;

    @FindBy(id = "inputFirstName")
    private WebElement firstnameField;

    @FindBy(id = "inputLastName")
    private WebElement lastnameField;

    @FindBy(id = "inputPassword")
    private WebElement passwordField;

    @FindBy(tagName = "button")
    private WebElement submitBtn;

    @FindBy(id="success")
    private WebElement successMsg;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    public void setUsernameField(String name) {
        usernameField.sendKeys(name);
    }

    public void setFirstnameField(String firstname) {
        firstnameField.sendKeys(firstname);
    }

    public void setLastnameField(String name) {
        lastnameField.sendKeys(name);
    }

    public void setPasswordField(String name) {
        passwordField.sendKeys(name);
    }

    public String getSuccessMessage(){
        return successMsg.getText();
    }

    public void submit() {
        submitBtn.click();
    }
}

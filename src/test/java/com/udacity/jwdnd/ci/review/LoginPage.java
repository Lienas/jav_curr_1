package com.udacity.jwdnd.ci.review;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    @FindBy(id = "inputUsername")
    private WebElement usernameField;

    @FindBy(id = "inputPassword")
    private WebElement passwordField;

    @FindBy(tagName = "button")
    private WebElement submitBtn;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
    }

    public void setUsernameField(String name) {
        usernameField.sendKeys(name);
    }

    public void setPasswordField(String name) {
        passwordField.sendKeys(name);
    }

    public void submit() {
        submitBtn.click();
    }

}

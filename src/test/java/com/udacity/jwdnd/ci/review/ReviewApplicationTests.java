package com.udacity.jwdnd.ci.review;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ReviewApplicationTests {

    @LocalServerPort
    private Integer port;

    private static WebDriver driver;

    @BeforeAll
    public static void beforeAll() {
        //get an chromedriver
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @AfterAll
    public static void afterAll() {
        driver.quit();
    }

    @Test
    public void testSignUp() throws InterruptedException {
        var username = "testuser";
        var password = "topsecret";

        //Step 1: SignUp
        driver.get("http://localhost:" + port + "/signup");
        SignUpPage signUpPage = new SignUpPage(driver);

        signUpPage.setUsernameField(username);
        signUpPage.setFirstnameField("Max");
        signUpPage.setLastnameField("Mustermann");
        signUpPage.setPasswordField(password);
        signUpPage.submit();
        var res = signUpPage.getSuccessMessage();
        assertEquals(res, "User successfully signed up");

        //Step 2: login
        driver.get("http://localhost:" + port + "/login");
        LoginPage loginPage = new LoginPage(driver);
        loginPage.setUsernameField(username);
        loginPage.setPasswordField(password);
        loginPage.submit(); // on Success redirects to chat

        //Step 3: add ChatMessage
        driver.get("http://localhost:" + port + "/chat");
        ChatPage chatPage = new ChatPage(driver);


        chatPage.setMessageTextField("This is a automated testmessage");
        chatPage.setMessageTextField("Say");
        chatPage.submit();

        var count = chatPage.getAmountOfMessages();
        assertTrue(count == 1);

        //Step 4: Logout
        chatPage.logout();

        //Step 5: Sign Up a different User
        username = "anotherUser";
        driver.get("http://localhost:" + port + "/signup");
        signUpPage.setUsernameField(username);
        signUpPage.setPasswordField(password);
        signUpPage.submit();

        //Step 6: Login as anotherUser
        driver.get("http://localhost:" + port + "/login");
        loginPage.setUsernameField(username);
        loginPage.setPasswordField(password);
        loginPage.submit();

        //Step 7: Submit response
        driver.get("http://localhost:" + port + "/chat");
        chatPage.setMessageTextField("This is my response");
        chatPage.setMessageTypeField("Say");
        chatPage.submit();
        count = chatPage.getAmountOfMessages();

        Thread.sleep(2000);

        assertTrue(count == 2);
        assertEquals(username,chatPage.getUserNameOfLastMsg());
    }

}

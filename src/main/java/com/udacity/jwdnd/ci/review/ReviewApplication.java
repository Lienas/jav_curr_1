package com.udacity.jwdnd.ci.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReviewApplication.class, args);
    }

//    @Bean
//    public String messageText() {
//        System.out.println("Hello Spring");
//        return "Hello Spring";
//    }
//
//    @Bean
//	public ChatMessage upperCaseMessage(MessageService messageService) {
//		System.out.println("uppercaseMessage");
//    	return messageService.uppercase();
//	}
//	@Bean
//	public ChatMessage lowerCaseMessage(MessageService messageService) {
//		System.out.println("loweCaseMessage");
//		return messageService.lowercase();
//	}

}


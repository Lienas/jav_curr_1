package com.udacity.jwdnd.ci.review.service;

import com.udacity.jwdnd.ci.review.mapper.MessagesMapper;
import com.udacity.jwdnd.ci.review.model.ChatForm;
import com.udacity.jwdnd.ci.review.model.ChatMessage;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    private MessagesMapper messagesMapper;


    public MessageService(MessagesMapper messagesMapper) {
        this.messagesMapper = messagesMapper;
    }

    public void addMessage(ChatForm chatForm) {
        ChatMessage newMessage = new ChatMessage();
        newMessage.setUsername(chatForm.getUsername());

        switch (chatForm.getMessageType()) {
            case "Say":
                newMessage.setMessage(chatForm.getMessageText());
                break;
            case "Shout":
                newMessage.setMessage(chatForm.getMessageText().toUpperCase());
                break;
            case "Whisper":
                newMessage.setMessage(chatForm.getMessageText().toLowerCase());
                break;
        }
       messagesMapper.addMessage(newMessage);
    }

    public List<ChatMessage> getChatMessages() {
        return messagesMapper.getAllMesages();
    }
}

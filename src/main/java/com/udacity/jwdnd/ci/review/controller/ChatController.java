package com.udacity.jwdnd.ci.review.controller;

import com.udacity.jwdnd.ci.review.model.ChatForm;
import com.udacity.jwdnd.ci.review.service.MessageService;
import com.udacity.jwdnd.ci.review.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/chat")
public class ChatController {

    private MessageService messageService;
    private UserService userService;

    public ChatController(MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @GetMapping
    public String getChatPage(ChatForm chatForm, Model model) {
        String userName =  userService.getLoggedInUser();
        model.addAttribute("chatMessages", this.messageService.getChatMessages());
        model.addAttribute("username", userName);
        return "chat";
    }

    @PostMapping
    public String postChatMessage(ChatForm chatForm, Model model) {
        // Logged in User can also get fetched from Bean Authentication !!!
        String userName =  userService.getLoggedInUser();
        chatForm.setUsername(userName);
        this.messageService.addMessage(chatForm);
        chatForm.setMessageText("");
        model.addAttribute("chatMessages", this.messageService.getChatMessages());
        model.addAttribute("username", userName);
        return "chat";
    }

    @ModelAttribute("allMessageTypes")
    public String[] allMessageTypes() {
        return new String[]{"Say", "Shout", "Whisper"};
    }

}

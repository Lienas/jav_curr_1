package com.udacity.jwdnd.ci.review.service;

import com.udacity.jwdnd.ci.review.mapper.UsersMapper;
import com.udacity.jwdnd.ci.review.model.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;

@Service
public class UserService {
    private final UsersMapper usersMapper;
    private final HashService hashService;

    public UserService(UsersMapper usersMapper, HashService hashService) {
        this.usersMapper = usersMapper;
        this.hashService = hashService;
    }

    public User getUser(String name) {

        return usersMapper.getUser(name);
    }

    public String getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof User) {
           return ((UserDetails)principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    public boolean isUserAvailable(String username) {

        boolean userIsAvaiable = usersMapper.getUser(username) == null;
        return userIsAvaiable;
    }

    /**
     *
     * @param user
     * @return Integer userId or Null if not successfull
     */
    public Integer addUser(User user) {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);

        String encodedSalt = Base64.getEncoder().encodeToString(salt);
        String hashedPassword = hashService.getHashedValue(user.getPassword(), encodedSalt);
        return usersMapper.addUser(
                new User(null,
                        user.getUserName(),
                        encodedSalt,
                        hashedPassword,
                        user.getLastName(),
                        user.getFirstName())
        );
    }

}

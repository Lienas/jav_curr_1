package com.udacity.jwdnd.ci.review.model;

public class ChatMessage {

    private Integer messageId;

    private String username;

    private String message;


    //<editor-fold desc="Getters and Setters">
    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }
    //</editor-fold>
}

package com.udacity.jwdnd.ci.review.controller;

import com.udacity.jwdnd.ci.review.model.User;
import com.udacity.jwdnd.ci.review.service.AuthenticationService;
import com.udacity.jwdnd.ci.review.service.UserService;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/signup")
public class SignupController {

    UserService userService;
    AuthenticationService authenticationService;

    public SignupController(UserService userService,
                            AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;

    }

    @GetMapping
    public String SignUpView(User user, Model model) {
        model.addAttribute("userData", new User(null,"","","","",""));
        return "signup";
    }

    @PostMapping
    public String RegisterUser(@ModelAttribute User user, Model model){

        model.addAttribute("userData", user);

        if (!userService.isUserAvailable(user.getUserName())) {
           model.addAttribute("errorMessage","User already exists");
           return "signup";
        }

        if (user.getUserName().isEmpty()) {
            model.addAttribute("errorMessage","Username is required");
            return "signup";
        }

        if (user.getPassword().isEmpty()) {
            model.addAttribute("errorMessage","Password is required");
            return "signup";
        }

        Integer userId= userService.addUser(user);

        if (userId == null) {
            model.addAttribute("errorMessage","something went wrong");
            return "signup";
        }
        model.addAttribute("successMessage", "User successfully signed up");
        return "signup";
    }
}

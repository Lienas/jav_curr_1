package com.udacity.jwdnd.ci.review.mapper;

import com.udacity.jwdnd.ci.review.model.ChatMessage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface MessagesMapper {

    @Select("Select * from MESSAGES where messageid = #{messageId}")
    ChatMessage getMessage(Integer messageId);

    @Select("SELECT messageId,username, messagetext as message from MESSAGES")
    List<ChatMessage> getAllMesages();

    @Insert("INSERT INTO MESSAGES (username, messagetext) " +
            "VALUES(#{username},#{message})")
    @Options(useGeneratedKeys = true, keyColumn = "messageid")
    Integer addMessage(ChatMessage chatMessage);

}

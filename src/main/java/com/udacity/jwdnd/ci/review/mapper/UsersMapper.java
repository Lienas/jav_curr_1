package com.udacity.jwdnd.ci.review.mapper;

import com.udacity.jwdnd.ci.review.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface UsersMapper {
    @Select("SELECT * FROM USERS where username=#{username}")
    User getUser(String username);

    @Insert("INSERT INTO USERS (username, salt, password, firstname,lastname ) " +
            "VALUES(#{userName},#{salt}, #{password}, #{firstName}, #{lastName})")
    @Options(useGeneratedKeys = true, keyColumn = "userid")
    Integer addUser(User user);
}
